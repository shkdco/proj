<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Do you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'descrtption',
            'text',
            'status.name',
            'category.name',
            'creator.name',
            //'updated_at',
            //'created_by',
            //'updated_by',
        ],
    ]) ?>

    <?php

foreach($model->tags as $tag){
    echo Html::a($tag->name,"",["class"=>"btn"]);
}

?>


<?php $form = ActiveForm::begin(); ?>
    <?php
    use kartik\rating\StarRating;
    use yii\web\JsExpression;
   
    
    
    // Fractional star rating with 6 stars , custom star symbol (uses glyphicon heart),
    // custom captions, and customizable ranges.
    echo $form->field($rating, 'rate')->widget(StarRating::className(), [
    //echo StarRating::widget(['name' => 'rating_19', 
        'pluginOptions' => [
            'stars' => 5, 
            'min' => 0,
            'max' => 5,
            'step' => 0.5,
            //'filledStar' => '<i class="glyphicon glyphicon-heart"></i>',
            //'emptyStar' => '<i class="glyphicon glyphicon-heart-empty"></i>',
            //'defaultCaption' => '{rating} hearts',
            //'starCaptions' => new JsExpression("function(val){return val == 1 ? 'One heart' : val + ' hearts';}")
        ]
    ]);
    
    echo "# of Raters: ".$model->getCount();

    ?>
        <div class="form-group">
        <?= Html::submitButton('Rate', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>


</div>
