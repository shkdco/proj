<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
         if (Yii::$app->user->can("authorRule"))
            echo Html::a('Create Article', ['create'], ['class' => 'btn btn-success']);
         ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            [
                'class' => 'yii\grid\ActionColumn',
             
                'visibleButtons' => [
                        
                        'update' => function ($model) {
                            return \Yii::$app->user->can('authorRule', ['post' => $model]);
                        },
                        'delete' => function ($model) {
                            return \Yii::$app->user->can('editorRule', ['post' => $model]);
                        },
            ]
                    ],
            
            

            //'id',
            'name',
            'descrtption',
            //'text',
            //'status_id',
            \Yii::$app->user->can('authorRule')?
            [   
                'attribute'=>'status_id',
                "value"=>"status.name",
            ]
            :
            [   
                'attribute'=>'Rating',
                "value"=>function($data){return $data->getRating(); } ,
            ],
            [   
                'attribute'=>'category_id',
                "value"=>"category.name",
            ],
            
            'created_at',
            'updated_at',

            [   
                'attribute'=>'created_by',
                "value"=>"creator.name",
            ],

            [   
                'attribute'=>'Rating',
                "value"=>function($data){return $data->getRating(); } ,
            ],
            //'updated_by',

            'tmpTag',

           
            


           ['class' => 'yii\grid\SerialColumn'],

            
        
        ],
        
    ]); ?>
</div>
