<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\Rating;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['authorRule'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['authorRule'],
                       // 'roleParams' => function() {
                       //     return ['article' => Article::findOne(['id' => Yii::$app->request->get('id')])];
                       // },                        
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['authorRule'],                        
                    ],                    
                ],
            ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }




    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();

        

        $searchModel->load(Yii::$app->request->get());

        if(\Yii::$app->user->can('authorRule')&& !\Yii::$app->user->can('editorRule')){
            $searchModel->created_by=Yii::$app->user->id;
        }
        
        if(!\Yii::$app->user->can('authorRule')){
            $searchModel->status_id=1;
        }

        

        $dataProvider = $searchModel->search([]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $rating = new Rating();
        $rating->article_id=$id;
        
        if ($rating->load(Yii::$app->request->post()) && $rating->save()) {
            return $this->redirect(['view', 'id' => $rating->article_id]);
        }
        $model=$this->findModel($id);
        $rating->rate=$model->getRating();

        //print_r($rating->errors);exit;
        return $this->render('view', [
            'model' =>$model ,
            'rating'=>$rating
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $model->status_id=4;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
            //exit;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
