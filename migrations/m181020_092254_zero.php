<?php

use yii\db\Migration;

/**
 * Class m181020_092254_zero
 */
class m181020_092254_zero extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        {
            $this->createTable('user', [
                'id' => $this->primaryKey(),
                'name'=> $this->string(),
                'username' => $this->string()->unique(),
                'password' => $this->string(),
                'auth_key' => $this->string(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ]);
        
            $this->createTable('article', [
                'id' => $this->primaryKey(),
                'name'=> $this->string(),
                'descrtption'=> $this->string(),
                'text'=> $this->text(),
                'status_id' => $this->integer(),
                'category_id' => $this->integer(),
                               
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ]);

            $this->createTable('rating', [
                'id' => $this->primaryKey(),
                'article_id'=> $this->integer(),
                'rate'=> $this->double(),
            ]);

            

            $this->createTable('status', [
                'id' => $this->primaryKey(),
                'name'=> $this->string(),
                //'created_at' => $this->timestamp(),
                //'updated_at' => $this->timestamp(),
                //'created_by' => $this->integer(),
                //'updated_by' => $this->integer(),
            ]);

            $this->createTable('tag', [
                'id' => $this->primaryKey(),
                'name'=> $this->string(),
                //'created_at' => $this->timestamp(),
                //'updated_at' => $this->timestamp(),
                //'created_by' => $this->integer(),
                //'updated_by' => $this->integer(),
            ]);

            $this->createTable('tagRel', [
                'article_id' => $this->integer(),
                'tag_id'=> $this->integer(),
                //'created_at' => $this->timestamp(),
                //'updated_at' => $this->timestamp(),
                //'created_by' => $this->integer(),
                //'updated_by' => $this->integer(),
            ]);

            $this->createTable('category', [
                'id' => $this->primaryKey(),
                'name'=> $this->string(),
                //'created_at' => $this->timestamp(),
                //'updated_at' => $this->timestamp(),
                //'created_by' => $this->integer(),
                //'updated_by' => $this->integer(),
            ]);
        }
    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181020_092254_zero cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181020_092254_zero cannot be reverted.\n";

        return false;
    }
    */
}
