<?php

use yii\db\Migration;

/**
 * Class m181020_094309_rbac
 */
class m181020_094309_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
            
        // add "author" role and give this role the "createPost" permission
        
       
        
        $adminRule = $auth->createPermission('adminRule');
        $editorRule = $auth->createPermission('editorRule');
        $authorRule = $auth->createPermission('authorRule');
        $userRule = $auth->createPermission('userRule');

        $admin = $auth->createRole('admin');
        $editor = $auth->createRole('editor');
        $author = $auth->createRole('author');
        $user = $auth->createRole('user');
        //$rule = new \yii\rbac\Rule;
        //$auth->add($rule);
        
        $auth->add($admin);  
        $auth->add($editor);
        $auth->add($author); 
        $auth->add($user); 

        $auth->add($adminRule);
        $auth->add($editorRule);
        $auth->add($authorRule);
        $auth->add($userRule); 
        
        
        $auth->addChild($adminRule, $editorRule);
        $auth->addChild($editorRule, $authorRule);
        $auth->addChild($authorRule, $userRule);


        //$rule = new \yii\rbac\Rule;
        //$auth->add($rule);
        //$rule = new \yii\rbac\Rule;
        //$auth->add($rule);
        //$rule = new \app\rbac\RRule;
        //$auth->add($rule);
        



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181020_094309_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181020_094309_rbac cannot be reverted.\n";

        return false;
    }
    */
}
