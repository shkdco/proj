<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface

{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }


    // public function getRole()
    // {
    //     return $this->hasOne(Auth_item::className(), ['' => '']);
    // }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name', 'username', 'password', 'auth_key'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            //'role' => 'Role',
        ];
    }





    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
       throw new NotSupportedException('This is not supported right now.. sorry');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
      * {@inheritdoc}
      */
      public function getId()
      {
          return $this->id;
      }
  
      /**
       * {@inheritdoc}
       */
      public function getAuthKey()
      {
          return $this->auth_key;
      }
  
      /**
       * {@inheritdoc}
       */
      public function validateAuthKey($authKey)
      {
          return $this->authKey === $authKey;
      }
  
      /**
       * Validates password
       *
       * @param string $password password to validate
       * @return bool if password provided is valid for current user
       */
      public function validatePassword($password)
      {
          //This is a Yii fuction that hashes a password coming from
          // the login form ans compares 
          return Yii::$app->security->validatePassword($password,$this->password);
      }    
     
      public function beforeSave($insert)
      {
         //we need to envoke the before save of the paernt 
         //and check for it's success before we can use our own
         //beforeSave() 
         if (parent::beforeSave($insert)) {
              if ($this->isNewRecord) {
                 // we populate the auth_key field only upon inserting a new record
                 // this value should not and therefore cannot be updated 
                 $this->auth_key = \Yii::$app->security->generateRandomString();
              }
              if ($this->isAttributeChanged('password'))
                 //We hash the password ONLY when the password is changed. 
                 //This can happen on a new record or when the user wants to change
                 // the password
                 // if it weren't for this if statment we would hash an already 
                 // hashed password
                 $this->password = Yii::$app->security->
                      generatePasswordHash($this->password);             
              return true;
          }
          return false;
      }    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['author_id' => 'id']);
    }

}
