<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use kartik\widgets\StarRating;
use yii\web\JsExpression;


/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $name
 * @property string $descrtption
 * @property string $text
 * @property int $status_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Article extends \yii\db\ActiveRecord
{

    public $tmpTag;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at',"tmpTag"], 'safe'],
            [['name', 'descrtption'], 'string', 'max' => 255],
            [['text'], 'string'],
        ];
    }

    public function behaviors()
    {
    return [
     
        [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ],
    BlameableBehavior::className(),
    ];
    }
    

        /**
    * @return \yii\db\ActiveQuery
    */
    /*public function getTag()
    {
        return $this->hasMany(Tag::className(), ['article_id' => 'id']);
    }*/


    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tagRel', ['article_id' => 'id']);
    }

    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Title',
            'descrtption' => 'Descrtption',
            'text' => 'Main Text',
            'status_id' => 'Status',
            'category_id' => 'Category',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
            'created_by' => 'Author',
            'updated_by' => 'Updated By',
            'tmpTag' => 'Tags',
        ];
    }

        
    public function getRating(){
    
        $rating=(new yii\db\Query)->select('avg(rate)')
        ->from('rating')
        ->where(["article_id"=>$this->id])
        ->scalar();
        if(is_null($rating))
            return 0;
        return round($rating,1);
    }

    public function getCount(){
        $rating=(new yii\db\Query)->select('count(rate)')
        ->from('rating')
        ->where(["article_id"=>$this->id])
        ->scalar();
        if(is_null($rating))
            return 0;
        return $rating;
    }

    public function afterFind()
      {
        parent::afterFind();

        $this->tmpTag=implode(",",ArrayHelper::map($this->tags,"name","name"));

      }
    public function afterSave($insert,$changedAttributes)
      {
         //we need to envoke the before save of the paernt 
         //and check for it's success before we can use our own
         //afterSave() 
         parent::afterSave($insert,$changedAttributes);
         
        $tags=TagRel::deleteAll(["article_id"=>$this->id]);

        $tmpTag=explode(",",$this->tmpTag);
        //echo 123;
        //print_r($tmpTag);

        foreach($tmpTag as $tag){
            $rTag=Tag::find()->where(["name"=>$tag])->one();
            if(is_null($rTag)){
                $rTag=new Tag(["name"=>$tag]);
                $rTag->save();
            }


            $tagRel = new TagRel();
            $tagRel->article_id=$this->id;
            $tagRel->tag_id=$rTag->id;
        
            $tagRel->save();
            //print_r($tagRel->errors);
        }
    
        return false;
      }
      
      


}
